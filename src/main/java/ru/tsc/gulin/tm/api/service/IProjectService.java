package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name);

    Project create(String name, String description);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project add(Project project);

    void clear();

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
