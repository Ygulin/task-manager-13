package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name);

    Project create(String name, String description);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project add(Project project);

    boolean existsById(String id);

    void clear();

}
