package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskFromProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
