package ru.tsc.gulin.tm.api.controller;

public interface IProjectTaskController {

    void addTaskToProjectById();

    void removeTaskFromProjectById();

    void removeProjectById();

}
